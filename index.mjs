import * as m from 'minecraft-protocol'
import { default as settings } from './settings.json' with {type: "json"}
import { generateUser } from './util/usergen.mjs'
import EventEmitter from 'node:events'
import * as fs from 'fs'

let bots = []

const botplug = [];

import { default as botplug_test } from "./plugins/testing.mjs" // We have to load plugins manually, because auto-detection does not work in this format
import { default as botplug_chat } from "./plugins/cq.mjs" 
import { default as botplug_chatParse } from "./plugins/!chat.mjs" 
import { default as botplug_player } from "./plugins/player.mjs" 
import { default as botplug_cmd } from "./plugins/command.mjs" 
import { default as botplug_cmdCore } from "./plugins/commandblock.mjs"
import { default as botplug_selfCare } from "./plugins/selfcare.mjs"
import { default as botplug_rejoin } from "./plugins/rejoin.mjs"
import { default as botplug_console } from "./plugins/console.mjs"
import { default as botplug_chatlog } from "./plugins/chatlog.mjs"
import { default as botplug_cloop } from "./plugins/cloop.mjs"

botplug.push(botplug_test)
botplug.push(botplug_chat)
botplug.push(botplug_chatParse)
botplug.push(botplug_player)
botplug.push(botplug_cmd)
botplug.push(botplug_cmdCore)
botplug.push(botplug_selfCare)
botplug.push(botplug_rejoin)
botplug.push(botplug_console)
botplug.push(botplug_chatlog)
botplug.push(botplug_cloop)

const loadplug = (botno) => {
  botplug.forEach((plug) => {
    try {
      if (plug.load) {
        plug.load(bots[botno])
      }
    } catch (e) { console.log(e) }
  })
}

const createBot = function createBot (host, oldId) {
  if (host.options.disabled) {
    return
  }
  const bot = new EventEmitter()
  bot._client = m.createClient({
    host: host.host,
    port: host.port ? host.port : 25565,
    username: generateUser(host.options.legalName),
    version: host.version ? host.version : settings.version_mc
  })
  if (typeof oldId !== 'undefined') {
    for (const i in bots[oldId].interval) {
      clearInterval(bots[oldId].interval[i])
    }
    delete bots[oldId]
    bot.id = oldId
    bots[oldId] = bot
  } else {
    bot.id = bots.length
    bots.push(bot)
  }
  
  bot.host = host
  bot.interval = {}

  bot.info = (msg) => {
    console.log(`[${bot.id}] [info] ${msg}`)
  }

  bot.displayChat = (type, msg) => {
    console.log(`[${bot.id}] [${type}] ${msg}`)
  }

  loadplug(bot.id)
  bot._client.on('error', (err) => {
    console.log(err)
  })
}

for (const i in settings.servers) {
  createBot(settings.servers[i])
}

export {
  bots,
  createBot
}
//module.exports.createBot = createBot
