export default {
  execute: (c) => {
    c.bot._client.end()
  },
  consoleIndex: true,
  level: 2
}
