export default {
  execute: (c) => {
    process.exit(1)
  },
  aliases: ['exit'],
  level: 2
}
