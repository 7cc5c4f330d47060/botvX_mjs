import { default as chatlog } from "../util/chatlog.mjs"
import { readdirSync, mkdirSync } from "fs"
import { default as settings } from '../settings.json' with {type: "json"}

const checkLog = () => {
  if (settings.disableLogging) return
  try {
    if (!readdirSync('.').includes('logs')) mkdirSync('logs')
    const dateToday = new Date(Date.now())
    const dateTomorrow = new Date(Date.now() + 86400000)
    const filenameToday = `${dateToday.getUTCMonth() + 1}-${dateToday.getUTCDate()}-${dateToday.getUTCFullYear()}`
    const filenameTomorrow = `${dateTomorrow.getUTCMonth() + 1}-${dateTomorrow.getUTCDate()}-${dateTomorrow.getUTCFullYear()}`
    if (!readdirSync('./logs').includes(filenameToday)) mkdirSync(`logs/${filenameToday}`)
    if (!readdirSync('./logs').includes(filenameTomorrow)) mkdirSync(`logs/${filenameTomorrow}`) // Create tomorrow's log directory early
  } catch (e) {
    console.log(e) // Prevents some crashes when disk space is full or when the permissions are incorrect
  }
}

setInterval(checkLog, 3600000) // Runs once every hour,
checkLog() // and at bot startup.

export default {
  load: (b) => {
    b.on('plainchat', (msg, type) => {
      if (!settings.disableLogging && !settings.disableChatLogging) chatlog(`chat_${b.host.host}_${b.host.port}`, `[${type}] ${msg}`)
    })
    b.on('command', (name, uuid, text) => {
      if (!settings.disableLogging && !settings.disableCommandLogging) chatlog(`cmd_${b.host.host}_${b.host.port}`, `${name} (${uuid}): ${text}`)
    })
  }
}
